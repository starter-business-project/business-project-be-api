export enum CoachType {
    OTHER = 'other',
    LIFE_STYLE = 'lifeStyle',
    SPORT = 'sport',
    NUTRITIONAL = 'nutritional',
    MENTAL = 'mental'
}