import {prop, Ref} from "typegoose";
import Item from "./Item";
import { CoachType } from '../constants/types.contants';
import Coach from "./Coach";

export default class Program extends Item {
    @prop({ enum: CoachType, default: CoachType.OTHER })
    programType?: string;

    @prop({ ref: Coach })
    coach?: Ref<Coach>;

    @prop({required: true})
    name?: string;

    @prop()
    duration?: Number;

    @prop()
    scheduling?: any;

    @prop({required: true})
    description?: string;
}
