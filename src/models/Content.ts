import {pre, prop, Ref} from "typegoose";
import Item from "./Item";
import * as getVideoId from 'get-video-id';
import Coach from "./Coach";

enum VideoType {
    OTHER = 'other',
    YOUTUBE = 'youtube',
    DAILYMOTION = 'dailymotion',
    VIMEO = 'vimeo',
}

enum ContentType {
    OTHER = 'other',
    VIDEO = 'video',
}

@pre<Item>('save', async function (next) {
    try {
        const video = getVideoId(this.url);
        if(video && video.service) {
            this.videoType = video.service;
            this.contentType = 'video';
        }
        return next();
    } catch (error) {
        return next(error);
    }
})

@pre<Item>('update', async function (next) {
    try {
        const { url } = this._update;
        const  video = getVideoId(url);
        if(video && video.service) {
            this._update.videoType = video.service;
            this._update.contentType = 'video';
        }
        return next();
    } catch (error) {
        return next(error);
    }
})

export default class Content extends Item {
    @prop({required: true})
    name?: string;

    @prop({required: true})
    description?: string;

    @prop({ enum: VideoType, default: VideoType.YOUTUBE })
    videoType?: string;

    @prop({ enum: ContentType, default: ContentType.VIDEO })
    contentType?: string;

    @prop({required: true})
    url?: string;

    @prop({ ref: Coach })
    coach?: Ref<Coach>;
}
