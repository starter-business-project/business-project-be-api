import User from "./User";
import {arrayProp, Ref} from "typegoose";
import Program from "./Program";
import Monitoring from "./Monitoring";
import Coach from "./Coach";
import Content from "./Content";

export default class Customer extends User {
    @arrayProp({ itemsRef: Monitoring })
    monitoring?: Ref<Monitoring>[];

    @arrayProp({ itemsRef: Program })
    programs?: Ref<Program>[];

    @arrayProp({ itemsRef: Coach })
    coaches?: Ref<Coach>[];

    @arrayProp({ itemsRef: Content })
    contents?: Ref<Content>[];
}
