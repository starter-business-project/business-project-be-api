import {arrayProp, prop, Ref} from "typegoose";
import User from "./User";
import Comment from "./Comment";
import Program from "./Program";
import Content from "./Content";
import { CoachType } from '../constants/types.contants';

export default class Coach extends User {
    @arrayProp({ items: String, enum: CoachType, default: CoachType.OTHER })
    coachType?: CoachType[];

    @prop()
    formation?: string;

    @prop()
    organization?: string;

    @prop()
    startActivity?: Date;

    @arrayProp({ itemsRef: Comment })
    comments?: Ref<Comment>[];

    // DEPRECATED :
    @arrayProp({ itemsRef: Content })
    contents?: Ref<Content>[];

    // DEPRECATED :
    @arrayProp({ itemsRef: Program })
    programs?: Ref<Program>[];
}
