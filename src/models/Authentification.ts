import * as bcrypt from 'bcrypt';
import {Request, Response} from "express";
import * as jwt from 'jsonwebtoken';
import getEnv from "../env";
import Coach from "./Coach";
import User from './User';
import Customer from "./Customer";

const env = getEnv();

const X_API_KEY_BO = env && env.X_API_KEY_BO;
const X_API_KEY_FO = env && env.X_API_KEY_FO;
const RANDOM_TOKEN_SECRET = env && env.RANDOM_TOKEN_SECRET;
const EXPIRATION = 3600;

export default class Authentification {
    private userModel = new User().getModelForClass(User);
    private coachModel = new Coach().getModelForClass(Coach);
    private customerModel = new Customer().getModelForClass(Customer);

    private getXApiKey = reqApiKey => {
        let result = null;
        switch (reqApiKey) {
            case X_API_KEY_BO:
                result = 'BO';
                break;
            case X_API_KEY_FO:
                result = 'FO';
                break;
            default:
                result = false;
        }
        return result;
    };

    private findAccountByEmail = async (xApiKey, email) => {
        const selectQuery = { select: 'email password isAuthorized status' };

        if(xApiKey === 'BO') {
            const { docs: users } = await this.userModel.getAll(null, selectQuery);
            const user = users && users.length > 0 && users.find(u => u.email === email);
            return { accountType: user ? 'user' : null, data: user };
        }
        let { docs: coaches } = await this.coachModel.getAll(null, selectQuery);
        const coach = coaches && coaches.length > 0 && coaches.find(c => c.email === email);
        if(coach) {
            return { accountType: coach ? 'coach' : null, data: coach };
        }

        let { docs: customers } = await this.customerModel.getAll(null, selectQuery);
        const customer = customers && customers.length > 0 && customers.find(c => c.email === email);
        if(customer) {
            return { accountType: customer ? 'customer' : null, data: customer };
        }
    };

    // TODO: USE IT FOR USER CREATING
    public existEmailOrUsername =  async (xApiKey, { email, userName }) => {
        const selectQuery = { select: 'email userName' };
        if(xApiKey === 'BO') {
            const { docs: users } = await this.userModel.getAll(null, selectQuery);
            const user = users.find(u => u.email === email || u.userName === userName);
            if(user) {
                return true;
            }
        }

        const { docs: coaches } = await this.coachModel.getAll(null, selectQuery);
        const coach = coaches.find(u => u.email === email || u.userName === userName);
        if(coach) {
            return true;
        }

        const { docs: customers } = await this.customerModel.getAll(null, selectQuery);
        const customer = customers.find(u => u.email === email || u.userName === userName);
        if(customer) {
            return true;
        }
    };

    public login = async (reqApiKey, { email, password }) => {
        const xApiKey = this.getXApiKey(reqApiKey);
        if(xApiKey) {
            const account = await this.findAccountByEmail(xApiKey, email);
            if(account) {
                const isValid = await bcrypt.compareSync(password, account.data.password);
                if(isValid) {
                    if(account.data.isAuthorized) {
                        return {
                            success: true,
                            token: jwt.sign({ userId: account.data._id }, RANDOM_TOKEN_SECRET,{ expiresIn: EXPIRATION }),
                            userId: account.data._id,
                            accountType: account.accountType
                        };
                    } else {
                        return {
                            success: false,
                            message: 'You are not authorized'
                        };
                    }
                } else {
                    return {
                        success: false,
                        message: 'Invalid password'
                    };
                }
            } else {
                return {
                    success: false,
                    message: 'Invalid email'
                };
            }
        } else {
            return {
                success: false,
                message: 'Invalid apiKey'
            };
        }
    };

    public register = async (reqApiKey, entity, payload) => {
        const xApiKey = this.getXApiKey(reqApiKey);
        if(xApiKey) {
            try {
                const existEmailOrUsername = payload && await this.existEmailOrUsername(xApiKey, payload);
                if (existEmailOrUsername) {
                    throw "Username or email are already used"
                } else {
                    const user = entity === 'customer'
                        ? await this.customerModel.addItem(payload)
                        : await this.coachModel.addItem(payload);

                    if (user) {
                        const {password} = payload;
                        const {email} = user;
                        return this.login(reqApiKey, {email, password});
                    }
                }
            } catch (e) {
                return new Error(e)
            }
        }
    };

    public checkToken = async (req: Request, res: Response, next): Promise<any> => {
        try {
            const reqToken = req.headers.authorization.split(' ')[1];
            const reqApiKey = req.headers['x-api-key'];

            jwt.verify(reqToken, RANDOM_TOKEN_SECRET, async (err) => {
                if(this.getXApiKey(reqApiKey)) {
                    if(!err) {
                        next();
                    } else {
                        res.status(401).send(err);
                    }
                } else {
                    res.status(401).send('Unauthorized !');
                }
            });
        } catch {
            res.status(401).json({
                error: new Error('Invalid request!')
            });
        }
    };

    public encryptPassword = async (reqApiKey, password) => {
        if(this.getXApiKey(reqApiKey)) {
            let salt = await bcrypt.genSaltSync(10);
            const encryptedPassword = await bcrypt.hashSync(password, salt);

            if(encryptedPassword) {
                return {
                    success: true,
                    data: encryptedPassword
                };
            } else {
                return {
                    success: false,
                    message: 'Failure for encrypt password'
                };
            }
        } else {
            return {
                success: false,
                message: 'Invalid apiKey'
            };
        }
    };
}
