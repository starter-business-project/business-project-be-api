import {arrayProp, pre, prop} from "typegoose";
import Item from "./Item";
import * as bcrypt from 'bcrypt';
import * as EmailValidator from 'email-validator';
import Address from "./Address";

enum Status {
    PENDING = 'pending',
    ARCHIVED = 'archived',
    SUSPENDED = 'suspended',
    ACTIVATED = 'activated',
}

enum Roles {
    ADMIN = 'admin',
    STAFF = 'staff',
    CUSTOMER = 'customer',
    GUEST = 'guest',
}

@pre<User>('save', async function (next) {
    try {
        if (this.isModified('password') || this.isNew) {
            let salt = await bcrypt.genSaltSync(10);
            this.password = await bcrypt.hashSync(this.password, salt);
        }
        return next();
    } catch (error) {
        return next(error);
    }
})

@pre<User>('update', async function (next) {
    try {
        const salt = await bcrypt.genSaltSync(10);
        const user = await this.model.getByID(this._update._id);
        const newPassword = this._update.password;
        const currentPassword = user && user.password;

        const isDifferent = newPassword !== currentPassword;
        if (newPassword && isDifferent) {
            this._update.password = await bcrypt.hashSync(newPassword, salt);
        }
        return next();
    } catch (error) {
        return next(error);
    }
})

export default class User extends Item {
    @prop({ unique: true, required: true, minlength: 5, match: /[0-9a-f]*/ })
    userName?: string;

    @prop({ required: true })
    firstName?: string;

    @prop({ required: true })
    lastName?: string;

    @prop()
    image?: string;

    @prop({required: true})
    birthDate?: Date;

    @prop()
    address?: Address;

    @prop()
    phone?: string;

    @prop()
    description?: string;

    @prop({ unique: true, required: true, validate: value => EmailValidator.validate(value) })
    email?: string;

    @prop({ required: true })
    isAuthorized?: boolean;

    @prop({ enum: Status, default: Status.PENDING })
    status?: string;

    @arrayProp({ items: String, enum: Roles, default: Roles.GUEST })
    roles?: Roles[];

    @prop({ required: true , minlength: 5 })
    password?: string;
}
