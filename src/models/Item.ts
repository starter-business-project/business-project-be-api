import {ModelType, prop, staticMethod, Typegoose, pre, plugin, InstanceType} from "typegoose";
import * as moment from 'moment-timezone';
import * as paginate from "mongoose-paginate-v2";
import * as aggregatePaginate from 'mongoose-aggregate-paginate-v2';
import queryHelpers from '../helpers/query.helpers';
import sublistQueries from '../queries/sublist.queries';
import listQueries from '../queries/list.queries';

@pre<Item>('save', async function (next) {
    try {
        this.creationAt = moment().tz("Europe/Paris");
        this.updateAt = moment().tz("Europe/Paris");
        return next();
    } catch (error) {
        return next(error);
    }
})

@pre<Item>('update', async function (next) {
    try {
        this._update.updateAt = moment().tz("Europe/Paris");
        return next();
    } catch (error) {
        return next(error);
    }
})

@plugin(paginate)
@plugin(aggregatePaginate)
export default class Item extends Typegoose {
    @prop({ unique: true, sparse: true })
    id?: string;

    @prop()
    creationAt?: Date;

    @prop()
    updateAt?: Date;

    @staticMethod
    static getMany(this: ModelType<Item> & typeof Item, ids: Array<string>, query: any, options: any) {
        const queryFormatted = queryHelpers.queriesInPartialString(query);
        return this.paginate( {...queryFormatted, _id: { $in: ids }}, options);
    }

    @staticMethod
    static getAll(this: ModelType<Item> & typeof Item, query: any, options: any) {
        const queryFormatted = queryHelpers.queriesInPartialString(query);
        return this.paginate(queryFormatted, options);
    }

    @staticMethod
    static getListByOid(this: ModelType<Item> & typeof Item, collections: string,  objectId: string, query: any, options: any) {
        const queryFormatted = queryHelpers.queriesInPartialString(query);
        return this.paginate( {...queryFormatted, [collections]: { $in: objectId } }, options );
    }

    @staticMethod
    static deleteItem(this: ModelType<Item> & typeof Item, id: string) {
        return this.findByIdAndRemove(id);
    }

    @staticMethod
    static getByID(this: ModelType<Item> & typeof Item, id: string, options: any) {
        return this.findById(id, options);
    }

    @staticMethod
    static updateItem(this: ModelType<Item> & typeof Item, id: string, item: Item) {
        const document = new this({ _id: id, ...item});
        return document.update(document);
    }

    @staticMethod
    static addItem(this: ModelType<Item> & typeof Item, item: Item) {
        return this.create(item);
    }

    @staticMethod
    static getStatsSublist(this: ModelType<Item> & typeof Item, sublistName: string) {
        return this.aggregate(sublistQueries.avgMinMax(sublistName));
    }

    @staticMethod
    static getRandomList(this: ModelType<Item> & typeof Item, size: number, options: any) {
        const aggregation = this.aggregate(listQueries.randomResult(size));
        return this.aggregatePaginate(aggregation, options);
    }
}
