import {arrayProp, prop, Ref} from "typegoose";
import Comment from "./Comment";
import Program from "./Program";
import Item from "./Item";

export default class Monitoring extends Item {
    @arrayProp({ itemsRef: Comment })
    comments?: Ref<Comment>[];

    @prop()
    program?: Program;
}
