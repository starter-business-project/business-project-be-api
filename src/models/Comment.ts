import User from "./User";
import {prop, Ref} from "typegoose";
import Item from "./Item";

enum Type {
    MONITORING = 'monitoring',
    RATE_COACH = 'rate_coach',
    USER = 'user',
}

export default class Comment extends Item {
    @prop({ required: true, ref: User })
    user?: Ref<User>;

    @prop({required: true})
    rate?: number;

    @prop({required: true})
    description?: string;

    @prop({ required: true, enum: Type, default: Type.USER })
    type?: string;
}
