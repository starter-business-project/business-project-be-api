import { prop } from "typegoose";
import Item from "./Item";

export default class Address extends Item {
    @prop({ required: true })
    street?: string;

    @prop({ required: true })
    city?: string;

    @prop({ required: true })
    zipCode?: string;

    @prop({ required: true })
    country?: string;
}
