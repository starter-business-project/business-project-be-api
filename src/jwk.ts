import * as jwt from 'express-jwt';
import * as jwksRsa from 'jwks-rsa';
import * as request from 'request';
import getEnv from './env';

const env = getEnv();

// DEPRECATED
export default {
    checking: () => jwt({
        secret: jwksRsa.expressJwtSecret({
            cache: true,
            rateLimit: true,
            jwksRequestsPerMinute: 5,
            jwksUri: `${env.AUTH0_BASE_URL}/.well-known/jwks.json`
        }),
        audience: env.AUTH0_AUDIENCE,
        issuer: `${env.AUTH0_BASE_URL}/`,
        algorithms: [env.AUTH0_ALGO]
    }),

    getToken: async () => {
        const options = {
            method: 'POST',
            url: `${env.AUTH0_BASE_URL}/oauth/token`,
            headers: {'content-type': 'application/x-www-form-urlencoded'},
            form: {
                grant_type: 'client_credentials',
                client_id: env.AUTH0_CLIENT_ID,
                client_secret: env.AUTH0_CLIENT_SECRET,
                audience: env.AUTH0_AUDIENCE
            }
        };

        const { access_token } = await new Promise((resolve, reject) => {
            request(options, async (error, response, body) => {
                if (error) reject(new Error(error)) ;
                resolve(JSON.parse(body));
            });
        });
        return access_token;
    }
}
