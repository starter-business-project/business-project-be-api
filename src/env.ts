import * as dotenv from 'dotenv';

export default () => {
    let envPath: string;
    switch (process.env.NODE_ENV) {
        case "test": envPath = ".env.test"; break;
        case "local-test": envPath = ".env.local.test"; break;
        case "dev-cloud": envPath = ".env.dev.cloud"; break;
        case "dev": envPath = ".env.dev"; break;
        default: envPath = ".env.prod";
    }
    process.env.NODE_ENV = process.env.NODE_ENV || "prod";

    return dotenv.config({
        path: envPath
    }).parsed;
}
