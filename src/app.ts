import * as express from "express";
import * as bodyParser from "body-parser";
import Routes from "./routes";
import * as mongoose from "mongoose";
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import * as cors from 'cors';
import getEnv from './env';

class App {
    public env: any;

    private user = process.env.MONGO_USER;
    private password = process.env.MONGO_PWD;
    private host = process.env.MONGO_HOST;
    private protocol = process.env.MONGO_PROTOCOL;

    public app: express.Application = express();
    private appListener: any;


    constructor() {
        this.env = getEnv();
        this.config();
        this.dbSetup();
        this.app.use('/api', Routes);
    }

    private config(): void{
        this.app.use(helmet());
        this.app.use(bodyParser.json( {limit: '50mb'}));
        this.app.use(cors());
        this.app.use(morgan('combined'));
        this.app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
        this.app.use(express.static('public'));
        this.appListener = this.app.listen(process.env.PORT);
    }

    private dbSetup(): void{
        const mongoUrl = this.user && this.password && this.host
            ? `${this.protocol}://${this.user}:${this.password}@${this.host}`
            : this.env.DB_HOST;

        mongoose.Promise = global.Promise;
        mongoose.connect(mongoUrl, {useNewUrlParser: true});
    }

    public closeDbConnection(): void {
        mongoose.disconnect();
    }

    public closeServer(): void {
        this.appListener.close();
    }
}

export const server = new App();
export default server.app;
