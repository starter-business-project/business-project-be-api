export default {
    avgMinMax: sublistName => [
        {
            $project: {
                sizeList: {
                    $cond: {
                        if: {
                            $isArray: `$${sublistName}`
                        },
                        then: {
                            $size: `$${sublistName}`
                        },
                        else: 0
                    }
                },
            }
        },
        {
            $group: {
                _id: null,
                group: { $sum: 1 },
                avg: { $avg: `$sizeList` },
                min: { $min: `$sizeList` },
                max: { $max: `$sizeList` },
            }
        }
    ]
}

