import { Request, Response } from 'express';
import Item from "../models/Item";

/**
 * @apiGroup Stats
 */
export default class StatsController<T extends Item> {

    private model: any;
    private entityName: string;

    constructor(entity: new () => T, entityName: string) {
        this.model = new entity().getModelForClass(entity);
        this.entityName = entityName;
    }

    /**
     * @api {get} /stats/{items}/sublist/:name Request for get stats about list
     * @apiName getStatsSublist
     * @apiGroup Stats
     *
     * @apiParam {Request} req.params.name is sublist name
     *
     * @apiSuccess {Object} res Return stats information.
     */
    public getStatsSublist = async (req: Request, res: Response): Promise<any> => {
        try {
            const result = await this.model.getStatsSublist(req.params.name);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: `${this.entityName} not found`,
                    data: null
                });
            }
            return res.status(200).send({
                success: true,
                data: result,
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };
}
