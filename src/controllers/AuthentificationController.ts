import {Request, Response} from "express";
import Authentification from "../models/Authentification";
import { validationResult } from 'express-validator';

/**
 * @apiGroup Auth
 */
export default class AuthentificationController {
    private model = new Authentification();

    /**
     * @api {get} /auth/ping Request for test API response
     * @apiName ping
     * @apiGroup Auth
     *
     * @apiSuccess {String} res Return "pong !".
     */
    public ping = async (req: Request, res: Response): Promise<any> => {
        try {
            return res.status(200).send({
                success: true,
                message: 'pong !',
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {post} /auth/login Request for login
     * @apiName login
     * @apiGroup Auth
     *
     * @apiExample Example body
     * {
     *       "email": "bernard.doe@test.fr",
     *       "password": "password89",
     * }
     *
     * @apiSuccess {String} res Return "pong !".
     */
    public login = async (req: Request, res: Response): Promise<any> => {
        try {
            const reqApiKey = req.headers['x-api-key'];
            const result = await this.model.login(reqApiKey, req.body);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: 'Email or password invalid',
                });
            } else {
                if (result.success) {
                    return res.status(200).send(result);
                } else {
                    return res.status(401).send(result);
                }
            }
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {post} /registration/:entity Request for register Customer or Coach
     * @apiName register
     * @apiGroup Auth
     *
     * @apiParam {Object} item Item data.
     * @apiExample Example body
     * {
     *       "firstName": "Bernardo",
     *       "lastName": "Doe",
     *       "email": "bernard.doe@test.fr",
     * }
     *
     * @apiSuccess {Object} res Return Customer or Coach information.
     */
    public register = async (req: Request, res: Response): Promise<any> => {
        try {
            const reqApiKey = req.headers['x-api-key'];
            const entity = req.params && req.params.entity;
            const errors = validationResult(req);
            if (errors.isEmpty()) {
                const result = await this.model.register(reqApiKey, entity, req.body);
                if (!result) {
                    return res.status(404).send({
                        success: false,
                        message: `${entity} not registered`,
                        data: null
                    });
                }
                if(result instanceof Error) {
                    return res.status(500).send(result.toString());
                }
                return res.status(200).send(result);
            } else {
                return res.status(422).send({
                    success: false,
                    message: `${entity} : failure for registering.`,
                    errors: errors.array()
                });
            }
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    public encryptPassword = async (req: Request, res: Response): Promise<any> => {
        try {
            const reqApiKey = req.headers['x-api-key'];
            const { password } = req.body;
            const result = await this.model.encryptPassword(reqApiKey, password);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: 'Failure for encrypt password',
                });
            } else {
                if (result.success) {
                    return res.status(200).send(result);
                } else {
                    return res.status(401).send(result);
                }
            }
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };
}
