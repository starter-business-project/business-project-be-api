import { Request, Response } from 'express';
import Item from "../models/Item";
import { validationResult } from 'express-validator';
import queryHelpers from '../helpers/query.helpers';

/**
 * @apiGroup CRUD
 */
export default class Controller<T extends Item> {

    private model: any;
    private entityName: string;

    constructor(entity: new () => T, entityName: string) {
        this.model = new entity().getModelForClass(entity);
        this.entityName = entityName;
    }

    /**
     * @api {delete} /{items}/:id Request delete Item
     * @apiName deleteItem
     * @apiGroup CRUD
     *
     * @apiParam {Request} req.params.id Item unique ID.
     *
     * @apiSuccess {Object} res Return Item information.
     */
    public deleteItem = async (req: Request, res: Response): Promise<any> => {
        try {
            const result = await this.model.deleteItem(req.params.id);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: `${this.entityName} not found`,
                    data: null
                });
            }
            return res.status(200).send({
                success: true,
                data: result,
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {post} /{items} Request Items list information according some ids
     * @apiName getMany
     * @apiGroup CRUD
     *
     * @apiExample Example body
     * [
     *  'RAZEZEZEZEZ6878767',
     *  'EEZE555ZEZEZEZEZEZ'
     *  'R88865454D55D5D6de'
     *]
     *
     * @apiSuccess {Object} res Return Items list data paginated.
     */
    public getMany = async (req: Request, res: Response): Promise<any> => {
        try {
            const { query, options } = queryHelpers.getQueryAndOptions(req);
            const result = await this.model.getMany(req.body, query, options);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: `${this.entityName}(s) not found`,
                    data: null
                });
            }
            return res.status(200).send({
                success: true,
                data: result,
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {get} /{items} Request Items list information
     * @apiName getAll
     * @apiGroup CRUD
     *
     * @apiSuccess {Object} res Return Items list data paginated.
     */
    public getAll = async (req: Request, res: Response): Promise<any> => {
        try {
            const { query, options } = queryHelpers.getQueryAndOptions(req);
            const result = await this.model.getAll(query, options);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: `${this.entityName}(s) not found`,
                    data: null
                });
            }
            return res.status(200).send({
                success: true,
                data: result,
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {get} /{items} Request Items list information according to ObjectID in sublist
     * @apiName getListByOid
     * @apiGroup CRUD
     *
     * @apiSuccess {Object} res Return Items list data paginated, according to ObjectID in sublist.
     */
    public getListByOid = async (req: Request, res: Response): Promise<any> => {
        try {
            const { collections, id } = req.params;
            const { query, options } = queryHelpers.getQueryAndOptions(req);
            const result = await this.model.getListByOid(collections, id, query, options);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: `${this.entityName}(s) not found`,
                    data: null
                });
            }
            return res.status(200).send({
                success: true,
                data: result,
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {get} /{items}/random/:size Request Items random list information
     * @apiName getRandomList
     * @apiGroup CRUD
     *
     * @apiSuccess {Object} res Return random Items list.
     */
    public getRandomList = async (req: Request, res: Response): Promise<any> => {
        try {
            const result = await this.model.getRandomList(req.params.size);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: `${this.entityName}(s) not found`,
                    data: null
                });
            }
            return res.status(200).send({
                success: true,
                data: result,
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {get} /{items}/:id Request Item information
     * @apiName getByID
     * @apiGroup CRUD
     *
     * @apiParam {Request} req.params.id Item unique ID.
     *
     * @apiSuccess {Object} res Return specific Item data.
     */
    public getByID = async (req: Request, res: Response): Promise<any> => {
        try {
            const options = req && req.query && req.query.select;
            const result = await this.model.getByID(req.params.id, options);
            if (!result) {
                return res.status(404).send({
                    success: false,
                    message: `${this.entityName} not found`,
                    data: null
                });
            }
            return res.status(200).send({
                success: true,
                data: result
            });
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {put} /{items}/:id Request update Item
     * @apiName updateItem
     * @apiGroup CRUD
     *
     * @apiParam {String} ID.
     * @apiParam {Object} item Item data.
     * @apiExample Example body
     * {
     *       "firstName": "Bernardi",
     *       "lastName": "Doe",
     *       "email": "bernard.doe@test.fr",
     * }
     *
     * @apiSuccess {Object} res Return Item information.
     */
    public updateItem = async (req: Request, res: Response): Promise<any> => {
        try {
            const errors = validationResult(req);
            if (errors.isEmpty()) {
                const result = await this.model.updateItem(req.params.id, req.body);
                if (!result) {
                    return res.status(404).send({
                        success: false,
                        message: `${this.entityName} not updated`,
                        data: null
                    });
                }
                return res.status(200).send({
                    success: true,
                    data: result
                });
            } else {
                return res.status(422).send({
                    success: false,
                    message: `${this.entityName} has errors.`,
                    errors: errors.array()
                });
            }
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };

    /**
     * @api {post} /{items} Request create Item
     * @apiName addItem
     * @apiGroup CRUD
     *
     * @apiParam {Object} item Item data.
     * @apiExample Example body
     * {
     *       "firstName": "Bernardo",
     *       "lastName": "Doe",
     *       "email": "bernard.doe@test.fr",
     * }
     *
     * @apiSuccess {Object} res Return Item information.
     */
    public addItem = async (req: Request, res: Response): Promise<any> => {
        try {
            const errors = validationResult(req);

            if (errors.isEmpty()) {
                const result = await this.model.addItem(req.body);
                if (!result) {
                    return res.status(404).send({
                        success: false,
                        message: `${this.entityName} not added`,
                        data: null
                    });
                }
                return res.status(200).send({
                    success: true,
                    data: result
                });
            } else {
                return res.status(422).send({
                    success: false,
                    message: `${this.entityName} has errors.`,
                    errors: errors.array()
                });
            }
        } catch (e) {
            res.status(500).send({
                success: false,
                message: e.toString(),
                data: null
            });
        }
    };
}
