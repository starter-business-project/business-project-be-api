export default {
    queriesInPartialString: queries => {
        let queryRegex = {};
        if(queries) {
            for(const key of Object.keys(queries)) {
                const value = queries[key];
                queryRegex[key] = new RegExp(value, 'i');
            }
        }
        return queryRegex;
    },
    getQueryAndOptions: payload => {
        if(payload) {
            const options = payload && payload.query && {
                page: payload.query.page || 1,
                limit: payload.query.limit || 10,
                select: payload.query.select || ''
            };
            const query = {...payload.query};
            delete query.page;
            delete query.limit;
            delete query.select;

            return { options, query }
        }
        return {}
    }
}
