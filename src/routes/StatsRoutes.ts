import { Router } from 'express';
import Item from "../models/Item";
import StatsController from "../controllers/StatsController";

export default class StatsRoutes<T extends Item> {

    private _route: Router = Router();
    private controller: StatsController<Item>;

    constructor(controller) {
        this.controller = controller;
        this.run();
    }

    private run(): void {
        this._route.get('/sublist/:name', this.controller.getStatsSublist);
    }

    get route(): Router {
        return this._route;
    }
}
