import { Router } from 'express';
import AuthentificationController from "../controllers/AuthentificationController";

export default class AuthentificationRoutes {

    private _route: Router = Router();
    private controller = new AuthentificationController();

    constructor() {
        this.run();
    }

    private run(): void {
        this._route.get('/ping', this.controller.ping);
        this._route.post('/login', this.controller.login);
        this._route.post('/encryptpassword', this.controller.encryptPassword);
        this._route.post('/registration/:entity', this.controller.register);
    }

    get route(): Router {
        return this._route;
    }
}
