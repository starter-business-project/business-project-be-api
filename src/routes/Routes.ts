import { Router } from 'express';
import Controller from "../controllers/Controller";
import Item from "../models/Item";
import { check } from 'express-validator';

export default class Routes<T extends Item> {

    private _route: Router = Router();
    private controller: Controller<Item>;

    constructor(controller) {
        this.controller = controller;
        this.run();
    }

    private run(): void {
        this._route.get('/:id', this.controller.getByID);
        this._route.get('/', this.controller.getAll);
        this._route.get('/random/:size', this.controller.getRandomList);
        this._route.get('/collections/:collections/:id', this.controller.getListByOid);
        this._route.delete('/:id', this.controller.deleteItem);
        this._route.put('/:id', this.checkBodyRequest(), this.controller.updateItem);
        this._route.post('/', this.checkBodyRequest(), this.controller.addItem);
        this._route.post('/many', this.controller.getMany);

    }

    private checkBodyRequest = () => [
        //check('updateAt').not().isEmpty().withMessage('updateAt must have a date'),
        //check('creationAt').not().isEmpty().withMessage('creationAt must have a date'),
    ];

    get route(): Router {
        return this._route;
    }
}
