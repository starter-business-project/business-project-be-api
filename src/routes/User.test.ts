import * as supertest from 'supertest';
import jwk from "../jwk";
import app, { server } from '../app';
import { expect }  from "chai";

describe('User',  () => {
    const userDataBodyMock = {
        roles: ["staff"],
        status: "pending",
        email: "test.unit@mail.com",
        firstName: "Johnny",
        lastName: "Cash",
        isAuthorized: false,
        connectionType: "default",
        userName: "unit-test06",
        password: "unitTest"
    };
    let userTest;

    /*afterAll(() => {
        server.closeServer();
        server.closeDbConnection();
        console.log('Connections closed');
    });*/

    describe('POST /users', () => {
        it('responds with json', async () => {
            const token = await jwk.getToken();

            const res = await supertest(app)
                .post(`/api/users`)
                .send(userDataBodyMock)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${token}`);

            expect(res.statusCode).to.equals(200);
            expect(res.body.success).to.equals(true);

            userTest = res && res.body && res.body.data;
        });
    });

    describe('GET /users', () => {
        it('responds with json', async () => {
            const token = await jwk.getToken();

            const res = await supertest(app)
                .get('/api/users')
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${token}`);

            expect(res.statusCode).to.equals(200);
            expect(res.body.success).to.equals(true);
        });
    });

    describe('GET /users/:id', () => {
        it('responds with json', async () => {
            const token = await jwk.getToken();

            const { body: { data: users } } = await supertest(app)
                .get('/api/users')
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${token}`);


            const res = await supertest(app)
                .get(`/api/users/${users[0]._id}`)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${token}`);

            expect(res.statusCode).to.equals(200);
            expect(res.body.success).to.equals(true);
        });
    });

    describe('PUT /users/:id', () => {
        it('responds with json', async () => {
            const token = await jwk.getToken();

            const userTestUpdating = {
                ...userTest,
                firstName: "John",
                lastName: "Cashy",
            };

            const res = await supertest(app)
                .put(`/api/users/${userTestUpdating._id}`)
                .send(userTestUpdating)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${token}`);

            expect(res.statusCode).to.equals(200);
            expect(res.body.success).to.equals(true);
        });
    });

    describe('DELETE /users/:id', () => {
        it('responds with json', async () => {
            const token = await jwk.getToken();

            const res = await supertest(app)
                .delete(`/api/users/${userTest._id}`)
                .set('Authorization', `Bearer ${token}`);

            expect(res.statusCode).to.equals(200);
            expect(res.body.success).to.equals(true);
        });
    });
});
