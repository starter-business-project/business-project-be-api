import { Router } from 'express';

import Routes from './Routes';
import StatsRoutes from './StatsRoutes';
import AuthentificationRoutes from './AuthentificationRoutes';

import Controller from "../controllers/Controller";
import StatsController from "../controllers/StatsController";
import Authentification from '../models/Authentification';

// Entities
import User from "../models/User";
import Coach from "../models/Coach";
import Customer from "../models/Customer";
import Comment from "../models/Comment";
import Program from "../models/Program";
import Content from "../models/Content";

const auth = new Authentification();
const route: Router = Router();
const authentification: Router = new AuthentificationRoutes().route;

// Routes
const users: Router = new Routes(new Controller<User>(User, 'User')).route;
const coaches: Router = new Routes(new Controller<Coach>(Coach, 'Coach')).route;
const customers: Router = new Routes(new Controller<Customer>(Customer, 'Customer')).route;
const comments: Router = new Routes(new Controller<Comment>(Comment, 'Comment')).route;
const programs: Router = new Routes(new Controller<Program>(Program, 'Program')).route;
const contents: Router = new Routes(new Controller<Content>(Content, 'Content')).route;

const statsCoaches: Router = new StatsRoutes(new StatsController<Coach>(Coach, 'Coach')).route;
const statsCustomers: Router = new StatsRoutes(new StatsController<Customer>(Customer, 'Customer')).route;

route.use('/auth', authentification);
route.use('/users', auth.checkToken, users);
route.use('/coaches', auth.checkToken, coaches);
route.use('/customers', auth.checkToken, customers);
route.use('/comments', auth.checkToken, comments);
route.use('/programs', auth.checkToken, programs);
route.use('/contents', auth.checkToken, contents);

route.use('/stats/coaches', auth.checkToken, statsCoaches);
route.use('/stats/customers', auth.checkToken, statsCustomers);

export default route;
