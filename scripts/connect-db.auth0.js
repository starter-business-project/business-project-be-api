function login(email, password, callback) {
    const mongo = require('mongodb');
    const bcrypt = require('bcrypt');

    const {dbname, dbuser, dbpassword, dbhost} = configuration;
    const uri = `mongodb://${dbuser}:${dbpassword}@${dbhost}`;

    mongo.MongoClient.connect(uri, (err, client) => {
        const db = client.db(dbname);
        const users = db.collection('users');

        users.findOne({ email }, function (err, user) {
            if (err) return callback(err);
            if (!user) return callback(new WrongUsernameOrPasswordError(email));
            if (!user.isAuthorized) return callback({message: 'User not authorized'});
            if (user.status !== 'activated') return callback({message: 'User not activated'});

            if(user.connectionType === 'default') {
                bcrypt.compare(password, user.password, function (err, isValid) {
                    if (err || !isValid) return callback(err || new WrongUsernameOrPasswordError(email));

                    return callback(null, {
                        user_id: user._id.toString(),
                        nickname: user.nickname,
                        email: user.email
                    });
                });
            }
        });
    });
}
