function authRule (userSelected, context, callback) {
    const mongo = require('mongodb');

    const {dbname, dbuser, dbpassword, dbhost} = configuration;
    const uri = `mongodb://${dbuser}:${dbpassword}@${dbhost}`;

    mongo.MongoClient.connect(uri, (err, client) => {
        const db = client.db(dbname);
        const users = db.collection('users');

        users.findOne({ email: userSelected.email }, function (err, user) {
            if (err) return callback(err);
            if (!user) return callback({message: 'User denied'});
            if (!user.isAuthorized) return callback({message: 'User not authorized'});
            if (user.status !== 'activated') return callback({message: 'User not activated'});
            return callback(null, userSelected, context);
        });
    });
}
