define({ "api": [
  {
    "group": "Auth",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "src/controllers/AuthentificationController.ts",
    "groupTitle": "Auth",
    "name": ""
  },
  {
    "type": "post",
    "url": "/auth/login",
    "title": "Request for login",
    "name": "login",
    "group": "Auth",
    "examples": [
      {
        "title": "Example body",
        "content": "{\n      \"email\": \"bernard.doe@test.fr\",\n      \"password\": \"password89\",\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "res",
            "description": "<p>Return &quot;pong !&quot;.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/AuthentificationController.ts",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/auth/ping",
    "title": "Request for test API response",
    "name": "ping",
    "group": "Auth",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "res",
            "description": "<p>Return &quot;pong !&quot;.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/AuthentificationController.ts",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/registration/:entity",
    "title": "Request for register Customer or Coach",
    "name": "register",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "item",
            "description": "<p>Item data.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example body",
        "content": "{\n      \"firstName\": \"Bernardo\",\n      \"lastName\": \"Doe\",\n      \"email\": \"bernard.doe@test.fr\",\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return Customer or Coach information.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/AuthentificationController.ts",
    "groupTitle": "Auth"
  },
  {
    "group": "CRUD",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD",
    "name": ""
  },
  {
    "type": "post",
    "url": "/{items}",
    "title": "Request create Item",
    "name": "addItem",
    "group": "CRUD",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "item",
            "description": "<p>Item data.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example body",
        "content": "{\n      \"firstName\": \"Bernardo\",\n      \"lastName\": \"Doe\",\n      \"email\": \"bernard.doe@test.fr\",\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return Item information.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "type": "delete",
    "url": "/{items}/:id",
    "title": "Request delete Item",
    "name": "deleteItem",
    "group": "CRUD",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Request",
            "optional": false,
            "field": "req.params.id",
            "description": "<p>Item unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return Item information.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "type": "get",
    "url": "/{items}",
    "title": "Request Items list information",
    "name": "getAll",
    "group": "CRUD",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return Items list data paginated.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "type": "get",
    "url": "/{items}/:id",
    "title": "Request Item information",
    "name": "getByID",
    "group": "CRUD",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Request",
            "optional": false,
            "field": "req.params.id",
            "description": "<p>Item unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return specific Item data.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "type": "get",
    "url": "/{items}",
    "title": "Request Items list information according to ObjectID in sublist",
    "name": "getListByOid",
    "group": "CRUD",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return Items list data paginated, according to ObjectID in sublist.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "type": "post",
    "url": "/{items}",
    "title": "Request Items list information according some ids",
    "name": "getMany",
    "group": "CRUD",
    "examples": [
      {
        "title": "Example body",
        "content": "[\n 'RAZEZEZEZEZ6878767',\n 'EEZE555ZEZEZEZEZEZ'\n 'R88865454D55D5D6de'\n]",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return Items list data paginated.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "type": "get",
    "url": "/{items}/random/:size",
    "title": "Request Items random list information",
    "name": "getRandomList",
    "group": "CRUD",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return random Items list.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "type": "put",
    "url": "/{items}/:id",
    "title": "Request update Item",
    "name": "updateItem",
    "group": "CRUD",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ID.",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "item",
            "description": "<p>Item data.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example body",
        "content": "{\n      \"firstName\": \"Bernardi\",\n      \"lastName\": \"Doe\",\n      \"email\": \"bernard.doe@test.fr\",\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return Item information.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/Controller.ts",
    "groupTitle": "CRUD"
  },
  {
    "group": "Stats",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "src/controllers/StatsController.ts",
    "groupTitle": "Stats",
    "name": ""
  },
  {
    "type": "get",
    "url": "/stats/{items}/sublist/:name",
    "title": "Request for get stats about list",
    "name": "getStatsSublist",
    "group": "Stats",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Request",
            "optional": false,
            "field": "req.params.name",
            "description": "<p>is sublist name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "res",
            "description": "<p>Return stats information.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/controllers/StatsController.ts",
    "groupTitle": "Stats"
  }
] });
