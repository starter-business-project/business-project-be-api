# Business Project / BE / API

RESTful Web APIs with Node.js, Express, MongoDB, Mongoose, TypeScript, and Typegoose

## Requirements

[NodeJS](https://nodejs.org/en/)

Install global TypeScript and TypeScript Node

```
npm install -g typescript ts-node
```

## Getting Started


Then install the dependencies

```
npm install
```

## Start the server

Run in development mode

```
npm run dev
```

Run in production mode 

```
npm run start
```

Run tests

```
npm run test
```

## Project architecture

API, FO and BO Hosted with Heroku in CI process.

#### Database
- Description: Database cloud-hosted
- Main libraries: MongoDB Atlas (Cloud)

#### API
- URL: https://business-project-be-api.herokuapp.com
- Gitlab : https://gitlab.com/starter-business-project/business-project-be-api
- Description: API REST
- Main libraries: Typegoose / Mongoose, Express.js

#### Front-Office (FO) - on going
- URL: http://business-project-fe-fo.herokuapp.com
- Gitlab : https://gitlab.com/starter-business-project/business-projet-fe-fo
- Description: For administer the data. And handle accounts granting
- Main libraries: Vue.js, Vuetify, Vuex

#### Back-Office (BO)
- URL: http://business-project-fe-bo.herokuapp.com
- Gitlab : https://gitlab.com/starter-business-project/business-project-fe-bo
- Description: Especially application for customers
- Main libraries: Vue.js, Vuetify, Vuex

#### business-project-vue-library
- Gitlab : https://gitlab.com/starter-business-project/business-projet-library-vue
- Description: Components library shared with FO and BO. It installed with NPM from Gitlab.
- Main libraries: Vue.js, Vuetify, Storybook (Test components)

![alt project architecture](docs/arch-business_project.png)
